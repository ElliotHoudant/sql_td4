

# TD sql advanced

A rendre pour le 30/10/2022

## Consignes générales

-   Ajouter le projet sur git : fork, git clone, git push (une fois terminé)
-   Chaque résultat doit être exporté en .csv, renommé avec le numero de la question, et enregistré dans un dossier 'resultats'
-   Les deux scripts et le modèle de données doivent également être enregistrés dans le dossier

### Données

-   Contact tracing : données de suivi des personnes
    -   Person.csv : 1 ligne par patient (id, nom, status de santé)
    -   Visit.csv : 1 ligne par visite d'un lieu (id de la visite, id du
        patient visiteur, id de l'endroit de la visite, date de début de
        la visite, date de fin de la visite)
    -   Place.csv : 1 ligne par lieu (id, nom du lieu, type du lieu)

### Modèle de données

-   Modéliser la base de données.

### Script DDL

-   Creation des tables avec clés primaires, clés étrangères
-   Insertion des données à partir des csv (en ligne de commande)

### Requetes

#### Niveau 1

1. Combien y a-t'il de places ?
2. Combien y a-t'il de "Theater" ?
3. Afficher les places de type "Grocery shop" et "Restaurant" ?
4. Combien y a t'il de types de places différents ?
5. Compter le nombre de places, par type de place.
6. Combien y a t'il eu de visites de la "Place nr 36" ?
7. Combien y a t'il eu de visites par type de place ?
8. Combien de personnes ont visité la "Place nr 36" ?
9. Combien de personnes étaient malades au moment où elles ont visité la "Place nr 36" ?
10. Quels sont les lieux qu'a visité Landy Greer (avec le nom, et la date de début et de fin de visites) ?

#### Niveau 2

11. Quels sont les noms et les statuts de santé des personnes que Landyn Greer a croisé (dans un même
    lieu, au même moment) ?
12. Quels sont les noms des personnes malades qui se sont croisés dans un bar ?
13. Classer les places par nombre décroissant de malades qui les ont visités, avec la durée moyenne de leurs visites dans cet endroit
14. Quels sont les couples de personnes malades / non malades qui se sont croisés ?
